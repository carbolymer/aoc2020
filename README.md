# Advent Of Code

To run only single day spec use:
```sh
cabal test --test-options='-m /Y2022.D01/'
```

To run single spec on file change:
```sh
while true; do cabal test --test-options='-m /Y2022.D01/'; inotifywait -e close_write -r .; done
```

To run all specs:
```sh
cabal test
```
