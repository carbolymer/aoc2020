module Y2020.D07Spec where

import           Common
import           Data.Either.Combinators       (mapLeft)
import           Data.List
import           Data.Void
import           Test.Hspec
import           Text.InterpolatedString.Perl6
import           Text.Megaparsec
import           Text.Megaparsec.Char

type Bag = (String, [String])

parseLine :: Parser Bag
parseLine = do
  bagColour <- colorP
  _ <- string " contain "
  manyBags <- string "no other bags" *> pure [] <|> manyBagsP
  _ <- char '.'
  pure (bagColour, manyBags)
    where
      colorP :: Parser String
      colorP = some lowerChar <> string " " <> some lowerChar <* char ' ' <* bagP
      manyBagsP :: Parser [String]
      manyBagsP = fmap concat $ flip sepBy1 ", " $ do
        nBags <- readInt <$> some digitChar <* char ' '
        colour <- colorP
        pure $ replicate nBags colour
      bagP = string "bag" <* optional (char 's')

parseLines :: Parser [Bag]
parseLines = sepBy parseLine eol

parseBags :: String -> Either String [Bag]
parseBags = mapLeft errorBundlePretty . parse parseLines "" . trim

validBags :: [Bag] -> String -> [Bag]
validBags bags colour = (filterBags bags colour) <> concat (map (validBags bags . fst) (filterBags bags colour))
  where
    filterBags bags_ colour_ = filter (hasBag colour_) bags_
    hasBag colour_ (_, contents) = (length $ filter (==colour_) contents) > 0

countBags :: [Bag] -> String -> Int
countBags bags = length . nub . sort . validBags bags

findAllBags :: [Bag] -> String -> [Bag]
findAllBags bags colour = subBags <> (concat $ map (findAllBags bags . fst) subBags)
  where
    subBags = concat . map matchingBags . concat . map snd $ matchingBags colour
    matchingBags colour_ = filter ((== colour_) . fst) bags


testInput :: String
testInput = [qc|light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.
|]

testInput2 :: String
testInput2 = [qc|shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.
|]

spec :: Spec
spec = do
  describe "parsers sample input" $ do
    it "check how many bags contain shiny gold" $ do
      case parseBags testInput of
        Left err  -> putStr $ err
        Right bags -> do
          -- putStr $ show bags
          countBags bags "shiny gold" `shouldBe` 4
    it "check how many other bags shiny gold contains" $ do
      case parseBags testInput2 of
        Left err  -> putStr $ err
        Right bags -> do
          putStr $ show bags
          (length $ findAllBags bags "shiny gold") `shouldBe` 126

  describe "run puzzle input" $ do
    input <- runIO $ readFile "resources/2020/d07.txt"
    let bags = case parseBags input of
                 Left err    -> error err
                 Right bags_ -> bags_
    it "---PART ONE---" $ do
      putStrLn $ "bags: " <> show (countBags bags "shiny gold")
    it "---PART TWO---" $ do
      putStrLn $ "bags: " <> show (length $ findAllBags bags "shiny gold")

