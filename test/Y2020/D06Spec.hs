module Y2020.D06Spec where

import           Data.List                     hiding (group)
import           Data.List.Split
import           Data.Maybe
import           Test.Hspec
import           Text.InterpolatedString.Perl6

countUnique :: String -> [Int]
countUnique input = map countUnique' groups
  where
    groups =  "\n\n" `splitOn` input
    countUnique' = length . nub . sort . filter (/= '\n')

countSame :: String -> [Int]
countSame input = map countSame' groups
  where
    groups =  "\n\n" `splitOn` input
    countSame' = length . nub . sort . retainSame
    retainSame group = fromJust . foldl retainSame' Nothing $ ("\n" `splitOn` group)
    retainSame' acc'm answers = pure . maybe answers (intersect answers) $ acc'm

testInput :: String
testInput = [qc|abc

a
b
c

ab
ac

a
a
a
a

b|]

testInput2 :: String
testInput2 = [qc|g
g
g
g
g|]

testInput3 :: String
testInput3 = [qc|g
i|]

spec :: Spec
spec = do
  describe "count unique per group" $ do
    it "part one" $ do
      countUnique testInput `shouldBe` [3, 3, 3 ,1, 1]
    it "count same" $ do
      countSame testInput `shouldBe` [3, 0, 1, 1, 1]
    it "same 5 letters" $ do
      countSame testInput2 `shouldBe` [1]
    it "one letter" $ do
      countSame "a" `shouldBe` [1]
    it "two different letters" $ do
      countSame "ab" `shouldBe` [2]
    it "two different letters2" $ do
      countSame testInput3`shouldBe` [0]

  describe "run puzzle input" $ do
    input <- runIO $ readFile "resources/2020/d06.txt"
    it "---PART ONE---" $ do
      putStrLn $ "sum of counts: " <> show (foldl (+) 0 $ countUnique input)
    it "---PART TWO---" $ do
      putStrLn $ "sum of counts: " <> show (foldl (+) 0 $ countSame input)

