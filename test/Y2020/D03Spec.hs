module Y2020.D03Spec where

import           Test.Hspec
import           Text.InterpolatedString.Perl6

data AreaMap = AreaMap Int Int [String]
  deriving (Show)

isTree :: AreaMap -> Int -> Int -> Bool
isTree (AreaMap _ _ area) right down = area !! down !! right == '#'

readMap :: String -> AreaMap
readMap input = AreaMap width height area
  where
    area = filter ((>0) . length) $ lines input
    width = length $ area !! 0
    height = length area

countTrees :: AreaMap -> Int -> Int -> Int
countTrees areaMap@(AreaMap width height _) right down = length . filter id $ map (uncurry (isTree areaMap)) indices
  where
    indices = takeWhile ((< height) . snd) [((i*right) `mod` width, i*down) | i <- [0..]]

testMap :: AreaMap
testMap = readMap [q|
..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#
|]

treesProduct :: AreaMap -> Int
treesProduct inputMap =
      (countTrees inputMap 1 1)
      * (countTrees inputMap 3 1)
      * (countTrees inputMap 5 1)
      * (countTrees inputMap 7 1)
      * (countTrees inputMap 1 2)

spec :: Spec
spec = do
  describe "run tobogan path" $ do
    it "traverses sample path r1 d1" $ do
      countTrees testMap 1 1 `shouldBe` 2
    it "traverses sample path r3 d1" $ do
      countTrees testMap 3 1 `shouldBe` 7
    it "traverses sample path r5 d1" $ do
      countTrees testMap 5 1 `shouldBe` 3
    it "traverses sample path r7 d1" $ do
      countTrees testMap 7 1 `shouldBe` 4
    it "traverses sample path r1 d2" $ do
      countTrees testMap 1 2 `shouldBe` 2
    it "verifies trees product" $ do
      treesProduct testMap `shouldBe` 336

  describe "run puzzle input" $ do
    it "" $ do
      inputMap <- readMap <$> readFile "resources/2020/d03.txt"
      let nTrees = countTrees inputMap 3 1
      putStrLn $ "---PART ONE---"
      putStrLn $ "number of trees: " <> show nTrees
      putStrLn $ "---PART TWO---"
      putStrLn $ "trees product: " <> show (treesProduct inputMap)

