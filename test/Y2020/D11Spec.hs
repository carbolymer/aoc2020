module Y2020.D11Spec where

import           Common
import           Test.Hspec
import           Text.InterpolatedString.Perl6


testInput :: String
testInput = [qc||]

spec :: Spec
spec = do
  describe "" $ do
    it "" $ do
      1 `shouldBe` 4

  describe "run puzzle input" $ do
    -- input <- runIO $ readFile "resources/d11.txt"
    it "---PART ONE---" $ do
      1 `shouldBe` 1
    it "---PART TWO---" $ do
      1 `shouldBe` 1

