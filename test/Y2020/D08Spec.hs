module Y2020.D08Spec where

import           Common                        hiding (count)
import           Control.Monad.State.Strict    hiding (state)
import           Data.Either.Combinators       (mapLeft)
import           Data.List                     (findIndices)
import           Data.Void
-- import           Debug.Trace
import           Test.Hspec
import           Text.InterpolatedString.Perl6
import           Text.Megaparsec               hiding (State)
import           Text.Megaparsec.Char

data Op = Acc Int | Jmp Int | Nop Int
  deriving Show

data ProgramResult = Termination Int | Loop Int
  deriving (Eq, Show)

parseOp :: Parser Op
parseOp = do
  opName <- count 3 lowerChar
  _ <- spaceChar
  signChar <- char '-' <|> char '+'
  value <- some digitChar
  let value' = (read value :: Int) * (if signChar == '-' then -1 else 1)
  case opName of
    "acc" -> pure $ Acc value'
    "jmp" -> pure $ Jmp value'
    "nop" -> pure $ Nop value'
    other -> fail other

parseProgram :: Parser [Op]
parseProgram = sepBy parseOp eol

readProgram :: String -> Either String [Op]
readProgram = mapLeft errorBundlePretty . parse parseProgram "" . trim

data ProgramState = ProgramState
  { visitedIndices :: ![Int]
  , accumulator    :: !Int
  } deriving (Show)

initialState :: ProgramState
initialState =  ProgramState [] 0

runProgram :: [Op] -> ProgramResult
runProgram program = evalState (go 0) initialState
  where
    go :: Int -> State ProgramState ProgramResult
    go currentIndex = do
      state@ProgramState{ visitedIndices, accumulator } <- get
      if currentIndex >= length program
        then pure $ Termination accumulator
        else if currentIndex `elem` visitedIndices
          then pure $ Loop accumulator
          else do
            put state{ visitedIndices = visitedIndices <> [currentIndex]}
            let op = program !! currentIndex
            case op of
              Acc v -> do
                modify $ \state'@ProgramState{ accumulator = accumulator' } ->
                  state'{ accumulator = accumulator' + v }
                go (currentIndex + 1)
              Jmp v -> go (currentIndex + v)
              Nop _ -> go (currentIndex + 1)


mutateProgram :: [Op] -> ([Op], ProgramResult)
mutateProgram program = head $ (nopMutated ++ jmpMutated)
  where
    nopMutated = filter (hasTerminated . snd) $ map (runMutatedProg nopToJmp) $ findIndices isNop program
    jmpMutated = filter (hasTerminated . snd) $ map (runMutatedProg jmpToNop) $ findIndices isJmp program
    runMutatedProg f ix = (updateWith program ix f, runProgram $ updateWith program ix f)
    isNop (Nop _) = True
    isNop _       = False
    isJmp (Jmp _) = True
    isJmp _       = False
    jmpToNop (Jmp v) = Nop v
    jmpToNop v       = v
    nopToJmp (Nop v) = Jmp v
    nopToJmp v       = v
    hasTerminated (Termination _) = True
    hasTerminated _               = False


testInput :: String
testInput = [qc|nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6|]

spec :: Spec
spec = do
  describe "parse sample input" $ do
    it "check accumulator value" $ do
      case readProgram testInput of
        Left err -> putStr err
        Right program ->
          runProgram program `shouldBe` Loop 5
    it "check if mutated program terminates" $ do
      case readProgram testInput of
        Left err -> putStr err
        Right program ->
          snd (mutateProgram program) `shouldBe` Termination 8

  describe "run puzzle input" $ do
    input <- runIO $ readFile "resources/2020/d08.txt"
    let program = case readProgram input of
                    Right p  -> p
                    Left err -> error err
    it "---PART ONE---" $ do
      putStrLn . show $ runProgram program
      True `shouldBe` True
    it "---PART TWO---" $ do
      putStrLn . show . snd $ mutateProgram program
      True `shouldBe` True

