module Y2020.D04Spec where

import           Common                        hiding (count)
import           Control.Monad
import           Data.Bifunctor
import           Data.Either
import           Data.List.Split               (splitOn, splitOneOf)
import qualified Data.Map.Strict               as M
import           Test.Hspec
import           Text.InterpolatedString.Perl6
import           Text.Parsec

data Passport = Passport
  { byr :: String
  , iyr :: String
  , eyr :: String
  , hgt :: String
  , hcl :: String
  , ecl :: String
  , pid :: String
  , cid :: Maybe String
  } deriving (Show)

readPassports :: Bool -> String -> [Either String Passport]
readPassports skipFieldValidation = map (readPassport skipFieldValidation) . filter (/= "") . splitOn "\n\n"

readPassport :: Bool -> String -> Either String Passport
readPassport skipFieldValidation input = Passport
    <$> parseF "byr" byrP
    <*> parseF "iyr" iyrP
    <*> parseF "eyr" eyrP
    <*> parseF "hgt" hgtP
    <*> parseF "hcl" hclP
    <*> parseF "ecl" eclP
    <*> parseF "pid" pidP
    <*> fieldM "cid"
  where
    byrP = do
      year <- fmap readInt $ count 4 digit
      unless (year >= 1920 && year <= 2002) . unexpected $ show year
      pure year
    iyrP = do
      year <- fmap readInt $ count 4 digit
      unless (year >= 2010 && year <= 2020) . unexpected $ show year
      pure year
    eyrP = do
      year <- fmap readInt $ count 4 digit
      unless (year >= 2020 && year <= 2030) . unexpected $ show year
      pure year
    hgtP = hgtPcm <|> hgtPin
      where
        hgtPcm = try $ do
          cm <- readInt <$> many1 digit <* string "cm"
          unless (cm >= 150 && cm <= 193) . unexpected $ show cm
          pure cm
        hgtPin = try $ do
          inch <- readInt <$> many1 digit <* string "in"
          unless (inch >= 59 && inch <= 76) . unexpected $ show inch
          pure inch
    hclP = string "#" *> count 6 hexDigit
    eclP = choice $ (try . string) <$> ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
    pidP = count 9 digit

    parseF :: (Show b) => String -> Parsec String () b -> Either String String
    parseF name parser = field name >>= parse'
      where
        parse' = if skipFieldValidation
                     then pure
                     else bimap show show . parse parser name

    field name = if name `M.member` fieldsMap
                    then Right $ fieldsMap M.! name
                    else Left $ "cannot read " <> name
    fieldM name = Right  $ fieldsMap M.!? name
    fieldsMap = M.fromList $ map fieldToPair fields
    fields = filter (/= "") $ "\n " `splitOneOf` input
    fieldToPair field' = if length nameValue == 2
                           then (nameValue !! 0, nameValue !! 1)
                           else error ("wtf: " <> input <> (show nameValue))
      where
        nameValue = splitOn ":" field'


testInput :: String
testInput =
  [qc|ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in|]

validPassports :: String
validPassports =
  [qc|pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719|]

invalidPassports :: String
invalidPassports =
  [qc|eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012 ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007|]

spec :: Spec
spec = do
  describe "test passports parsing" $ do
    it "verifies that in test data are 2 correct passports" $ do
      -- putStrLn . unlines . map show . lefts $ readPassports False testInput
      -- putStrLn . unlines . map show $ readPassports False testInput
      (length $ lefts $ readPassports False testInput) `shouldBe` 2
    it "verifies that passports are valid" $ do
      (length $ lefts $ readPassports False validPassports) `shouldBe` 0
    it "verifies that passports are invalid" $ do
      (length $ lefts $ readPassports False invalidPassports) `shouldBe` 4

  describe "run puzzle input" $ do
    input <- runIO $ readFile "resources/2020/d04.txt"
    it "---PART ONE---" $ do
      putStrLn $ "total passports:" <> (show . length $ splitOn "\n\n" input)
      putStrLn $ "valid passports: " <> show (length . rights $ readPassports True input)
    it "---PART TWO---" $ do
      putStrLn $ "valid passports: " <> show (length . rights $ readPassports False input)
      putStrLn $ "invalid passports: " <> show (length . lefts $ readPassports False input)

