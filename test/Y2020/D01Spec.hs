module Y2020.D01Spec where

import           Common
-- import           Data.List     (nub)
import           Test.Hspec

spec :: Spec
spec = describe "runs first day" $ do
  it "" $ do
    input <- map (read :: String -> Int) . lines <$> readFile "resources/2020/d01.txt"
    -- let report = nub [x*y*z | x <- input,
    --                           y <- input,
    --                           z <- input,
    --                           x + y + z == 2020]
    let sums2 = filter (\(a,b) -> a + b == 2020) $ subSeq2 input
    putStrLn $ "couples: " <> show sums2
             <> " products: " <> (show $ map (uncurry (*)) sums2)

    let sums3 = filter (\(a,b,c) -> a + b + c == 2020) $ subSeq3 input
    putStrLn $ "triplets: " <> (show sums3)
             <> " products: " <> (show $ map (uncurry3 mul3) sums3)

subSeq2 :: [b] -> [(b,b)]
subSeq2 (x:xs) = [(x,y) | y <- xs] ++ subSeq2 xs
subSeq2 []     = []

subSeq3 :: [b] -> [(b,b,b)]
subSeq3 (x:xs) = [(x,y,z) | (y, z) <- subSeq2 xs] ++ subSeq3 xs
subSeq3 []     = []


