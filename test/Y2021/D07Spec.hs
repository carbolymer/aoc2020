module Y2021.D07Spec where

import           Common
import           Data.List                     (sort, sortOn)
import           Data.List.Split               (splitOn)
import           Debug.Trace
import           Test.Hspec
import           Text.InterpolatedString.Perl6

spec :: Spec
spec = do
  describe "examples" $ do
    it "move crabs to minimal fuel position" $ do
      findMinFuelPosition testInput `shouldBe` (2, 37)
    it "move crabs to minimal fuel position 2" $ do
      findMinFuelPosition testInput2 `shouldBe` (2, 37)
    it "move crabs to minimal fuel position fuel squared" $ do
      findMinFuelPositionSq testInput `shouldBe` (5, 168)

  describe "run puzzle input" $ do
    input <- runIO $ readFile "resources/2021/d07.txt"
    let positions :: [Int] = read <$> splitOn "," input
    it "---PART ONE---" $ do
      findMinFuelPosition positions `shouldBe` (323,336040)
    it "---PART TWO---" $ do
      findMinFuelPositionSq positions `shouldBe` (463,94813675)

findMinFuelPosition :: [Int] -> (Int, Int)
findMinFuelPosition positions = (minPosition, totalFuel)
  where
    totalFuel = sum $ map (abs . (-) minPosition) positions
    minPosition = sort positions !! midPoint
    midPoint = truncate $ if odd $ length positions
                  then (fromIntegral $ length positions + 1 :: Double) / 2.0
                  else (fromIntegral $ length positions) / 2.0

findMinFuelPositionSq :: [Int] -> (Int, Int)
findMinFuelPositionSq positions = (minPosition, totalFuel)
  where
    totalFuel = sum [cost $ abs (minPosition - d) | d <- positions]
    minPosition = let (minPos', dist) = head diffMin in round $ minPos' + dist
    diffMin = traceShowId $ sortOn snd [ (d, abs (mean - d)) | d <- sortedPositions]
    mean = (fromIntegral $ sum positions) / (fromIntegral $ length positions) :: Double
    sortedPositions = map fromIntegral $ sort positions :: [Double]
    cost a = a * (a + 1) `div` 2 :: Int

testInput :: [Int]
testInput = [16,1,2,0,4,2,7,1,2,14]

testInput2 :: [Int]
testInput2 = [16,1,2,0,4,2,7,1,2,14,2]

