module Y2021.D05Spec where

import           Common
import           Test.Hspec
import           Text.InterpolatedString.Perl6

spec :: Spec
spec = do
  describe "" $ do
    it "" $ do
      1 `shouldBe` 4

  describe "run puzzle input" $ do
    -- input <- runIO $ readFile "resources/2021/d05.txt"
    it "---PART ONE---" $ do
      1 `shouldBe` 1
    it "---PART TWO---" $ do
      1 `shouldBe` 1

testInput = [qc||]
testInput :: String

