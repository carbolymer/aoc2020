module Y2021.D01Spec where

import           Test.Hspec

spec :: Spec
spec = do
  describe "sample input" $ do
    it "diff1" $ do
      countIncreases sampleInput `shouldBe` 7

  describe "run puzzle input" $ do
    input <- runIO $ readFile "resources/2021/d01.txt"
    let sweepResults = map read $ lines input :: [Int]
    it "---PART ONE---" $ do
      countIncreases sweepResults `shouldBe` 1121
    it "---PART TWO---" $ do
      countIncreases3 sweepResults `shouldBe` 1065


sampleInput :: [Int]
sampleInput = [ 199
              , 200
              , 208
              , 210
              , 200
              , 207
              , 240
              , 269
              , 260
              , 263 ]

countIncreases :: [Int] -> Int
countIncreases input = length $ filter id $ map isIncrease $ zip input (tail input)
  where
    isIncrease (a, b) = a - b < 0

countIncreases3 :: [Int] -> Int
countIncreases3 input = countIncreases w3
  where
    w3 = map sum' $ zip3 input (tail input) (tail $ tail input)
    sum' (a, b, c) = a + b + c
