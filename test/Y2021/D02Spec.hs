module Y2021.D02Spec where

import           Common
import           Test.Hspec
import           Text.InterpolatedString.Perl6
import Control.Monad.State.Strict
import Data.List (foldl')

spec :: Spec
spec = do
  describe "sample input" $ do
    it "sample input" $ do
      (positionProduct . runCommands False $ parseInputLines testInput) `shouldBe` 150
    it "sample input2" $ do
      (positionProduct . runCommands True $ parseInputLines testInput) `shouldBe` 900

  describe "run puzzle input" $ do
    input <- runIO $ readFile "resources/2021/d02.txt"
    it "---PART ONE---" $ do
      (positionProduct . runCommands False $ parseInputLines input) `shouldBe` 1882980
    it "---PART TWO---" $ do
      (positionProduct . runCommands True $ parseInputLines input) `shouldBe` 1971232560

positionProduct :: Position -> Int
positionProduct (Position horizontal depth _) = horizontal * depth

data Position = Position
  { horizontal :: Int
  , depth :: Int
  , aim :: Int
  }

data Command
  = Forward Int
  | Down Int
  | Up Int

runCommands :: Bool -> [Command] -> Position
runCommands useAim = foldl' runCmd (Position 0 0 0)
  where
    runCmd (Position horizontal depth aim) (Down v) = 
      if useAim
         then Position horizontal depth (aim + v)
         else Position horizontal (depth + v) (aim + v)
    runCmd (Position horizontal depth aim) (Up v) = 
      if useAim
         then Position horizontal depth (aim - v)
         else Position horizontal (depth - v) (aim - v)
    runCmd (Position horizontal depth aim) (Forward v) =
      if useAim
         then Position (horizontal + v) (depth + aim * v) aim
         else Position (horizontal + v) depth aim

parseInputLines :: String -> [Command]
parseInputLines = map parseCommand . lines

parseCommand :: String -> Command
parseCommand input =
  case commandStr of
    "forward" -> Forward value
    "up" -> Up value
    "down" -> Down value
    unknown -> error $ "unknown command: " <> unknown
  where
    [commandStr, valueStr] = words input
    value = read valueStr

testInput :: String
testInput = [qc|forward 5
down 5
forward 8
up 3
down 8
forward 2|]
