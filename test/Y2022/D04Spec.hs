module Y2022.D04Spec where

import Common
import Test.Hspec
import Text.InterpolatedString.Perl6
import Text.Megaparsec               hiding (State)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer qualified as L

spec :: Spec
spec = do
  describe "Validate sample input" $ do
    it "sample part 1" $ do
      (length . filter id . map isSubset $ readAssignments sampleInput) `shouldBe` 2

  describe "run puzzle input" $ do
    input <- runIO $ readFile "resources/2022/d04.txt"
    it "---PART ONE---" $ do
      (length . filter id . map isSubset $ readAssignments input) `shouldBe` 582
    it "---PART TWO---" $ do
      (length . filter id . map isOverlap $ readAssignments input) `shouldBe` 0

readAssignments :: String -> [((Int, Int), (Int, Int))]
readAssignments = map (parseThrow assignmentPairP) . lines
  where
    assignmentPairP :: Parser ((Int, Int), (Int, Int))
    assignmentPairP = do
      e1 <- (,) <$> L.decimal <*> (char '-' *> L.decimal)
      _ <- char ','
      e2 <- (,) <$> L.decimal <*> (char '-' *> L.decimal)
      pure (e1, e2)

isSubset :: ((Int, Int), (Int, Int)) -> Bool
isSubset ((e1f, e1t), (e2f, e2t)) = (e1f <= e2f && e1t >= e2t) || (e1f >= e2f && e1t <= e2t)

isOverlap :: ((Int, Int), (Int, Int)) -> Bool
isOverlap ((e1f, e1t), (e2f, e2t)) = (e1f >= e2f && e1f <= e2t) || (e2f >= e1f && e2f <= e1t) || (e1t >= e2f && e1t <= e2t) || (e2t >= e1f && e2t <= e1t)

sampleInput :: String
sampleInput = [qc|2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8|]
