module Y2022.D14Spec where

import Common
import Test.Hspec
import Text.InterpolatedString.Perl6
import Data.List.Extra (splitOn)
import Linear hiding (trace)
import Control.Monad (join)
import qualified Data.Vector as V
import Linear.V
import Control.Monad.Trans.State.Strict
import Control.Lens hiding (below, Empty)
import Data.Maybe (fromJust)
import Debug.Trace

spec :: Spec
spec = do
  describe "validate sample input" $ do
    it "sample part 1" $ do
      parseRockLines sampleInput `shouldBe` (V2 503 9, [[V2 498 4,V2 498 6,V2 496 6],[V2 503 4,V2 502 4,V2 502 9,V2 494 9]])
    it "solve sample input" $ do
      solve sampleInput `shouldBe` 24
    it "solve sample input 2" $ do
      solve2 sampleInput `shouldBe` 93

  describe "run puzzle input" $ do
    input <- runIO $ readFile "resources/2022/d14.txt"
    it "---PART ONE---" $ do
      solve input `shouldBe` 964
    it "---PART TWO---" $ do
      solve2 input `shouldBe` 0

parseRockLines :: String -> (V2 Int, [[V2 Int]])
parseRockLines rockLinesStr = do
  let rockLines = map (map (readVec . splitOn ",") . splitOn " -> ") $ lines rockLinesStr
      f (V2 maxx maxy) (V2 x y) = V2 (max maxx x) (max maxy y)
      dimensions = foldl f (V2 0 0) (join rockLines)
  (dimensions + V2 1 1, rockLines)
  where
    readVec [read -> x, read -> y] = V2 @Int x y
    readVec other = error $ "cannot read vec " <> show other

data CaveCell = Empty | Rock | Sand
  deriving (Eq, Ord, Show)

type Cave = [[CaveCell]]

mkCave :: V2 Int -> Cave
mkCave (V2 x y) = replicate y $ replicate x Empty

getCell :: V2 Int -> State Cave (Maybe CaveCell)
getCell = preuse . eAt

dropSand :: State Cave Bool
dropSand = dropSandFrom (V2 500 0)
  where
    dropSandFrom v = do
      let below = v + V2 0 1
          ld = v + V2 (-1) 1
          rd = v + V2 1 1
      cellD'm <- getCell below
      cellLd'm <- getCell ld
      cellRd'm <- getCell rd
      case (cellD'm, cellLd'm, cellRd'm) of
        (Nothing, Nothing, Nothing) -> pure False -- the void
        (Just Empty, _, _) -> dropSandFrom below
        (_, Just Empty, _) -> dropSandFrom ld
        (_, _, Just Empty) -> dropSandFrom rd
        (_, _, _) -> do
          eAt v .= Sand
          pure True

isInputClogged :: State Cave Bool
isInputClogged = do
  cell <- getCell (V2 500 0)
  pure $ cell == Just Sand

drawRockLine :: [V2 Int] -> State Cave ()
drawRockLine [] = pure ()
drawRockLine [r0] = eAt r0 .= Rock
drawRockLine (r0:r1:rs)
  | r0 == r1 = drawRockLine (r1:rs)
  | otherwise = do
    let step = signorm' $ r1 - r0
    drawRockLine [r0]
    drawRockLine ((r0 + step):r1:rs)
      where
        signorm' (V2 0 0) = error "cannot signorm zero vec"
        signorm' (V2 0 y)
          | y > 0 = V2 0 1
          | y < 0 = V2 0 (-1)
        signorm' (V2 x 0)
          | x > 0 = V2 1 0
          | x < 0 = V2 (-1) 0
        signorm' v = error $ "cannot signorm vec: " <> show v

solve :: String -> Int
solve inputStr = do
  let (dimensions, rockLines) = parseRockLines inputStr
      cave = mkCave dimensions
  (`evalState` cave) $ do
    mapM_ drawRockLine rockLines
    go (0 :: Int)
      where
        go n = do
          res <- dropSand
          if res
             then go (n + 1)
             else pure n

solve2 :: String -> Int
solve2 inputStr = do
  let (V2 depth height, rockLines) = parseRockLines inputStr
      caveDimensions@(V2 newDepth newHeight) = V2 (max ((height+3)*4) (max 2000 depth)) (height + 2)
      cave = mkCave caveDimensions
      rockLinesFloor = [V2 0 (newHeight-1), V2 (newDepth-1) (newHeight-1)]:rockLines
  (`evalState` cave) $ do
    mapM_ drawRockLine rockLinesFloor
    go (1 :: Int)
      where
        go n = do
          _ <- dropSand
          t <- isInputClogged
          if t
             then do
               -- rendered <- gets drawCave
               -- trace rendered $ pure n
               pure n
             else go (n + 1)


drawCell :: CaveCell -> Char
drawCell Empty = ' '
drawCell Rock = '█'
drawCell Sand = '░'

drawCave :: Cave -> String
drawCave = unlines . map (map drawCell)

sampleInput :: String
sampleInput = [qc|498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9|]
