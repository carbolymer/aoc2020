module Y2022.D02Spec where

import Common
import Test.Hspec
import Text.InterpolatedString.Perl6
import Data.List.Extra (enumerate)
import Control.Lens

spec :: Spec
spec = do
  describe "run sample input" $ do
    it "sample input" $ do
      calculateResult (parseInput testInput) `shouldBe` 15

  describe "run puzzle input" $ do
    input <- runIO $ readFile "resources/2022/d02.txt"
    it "---PART ONE---" $ do
      calculateResult (parseInput input) `shouldBe` 14297
    it "---PART TWO---" $ do
      calculateResult2 (parseInput2 input) `shouldBe` 10498

calculateResult :: [(Play, Play)] -> Int
calculateResult = foldl go 0
  where
    go acc play = calculatePlay play + acc

calculateResult2 :: [(Play, Ordering)] -> Int
calculateResult2 = foldl go 0
  where
    go acc play@(opponent, _) = calculatePlay (opponent, findPlay play) + acc


data Play = R | P | S
  deriving (Show, Eq, Enum, Bounded)

instance Ord Play where
  compare R R = EQ
  compare R P = LT
  compare R S = GT
  compare P R = GT
  compare P P = EQ
  compare P S = LT
  compare S R = LT
  compare S P = GT
  compare S S = EQ

findPlay :: (Play, Ordering) -> Play
findPlay (p, o) = do
  let searchResults = map (view _2) $ filter (\(p1, _, o') -> p1 == p && o == o') allPlays
  case searchResults of
    [play] -> play
    wtf -> error $ "not 1 play found: " <> show wtf

allPlays :: [(Play, Play, Ordering)]
allPlays = [(a, b, compare a b) | a <- enumerate, b <- enumerate]

calculatePlay :: (Play, Play) -> Int
calculatePlay (opponent, you) = do
  let yourPlayScore =
        case you of
           R -> 1
           P -> 2
           S -> 3
  yourPlayScore +
    case compare opponent you of
      GT -> 0
      EQ -> 3
      LT -> 6

decodePlay :: Char -> Play
decodePlay 'A' = R
decodePlay 'X' = R
decodePlay 'B' = P
decodePlay 'Y' = P
decodePlay 'C' = S
decodePlay 'Z' = S
decodePlay p   = error $ "unknown play: " <> [p]

decodeResult :: Char -> Ordering
decodeResult 'X' = GT
decodeResult 'Y' = EQ
decodeResult 'Z' = LT
decodeResult p   = error $ "unknown result: " <> [p]

parseInput :: String -> [(Play,Play)]
parseInput = map parseLine . lines
  where
    parseLine [a, ' ', b] = (decodePlay a, decodePlay b)
    parseLine line        = error $ "unknown line" <> line

parseInput2 :: String -> [(Play,Ordering)]
parseInput2 = map parseLine . lines
  where
    parseLine [a, ' ', b] = (decodePlay a, decodeResult b)
    parseLine line        = error $ "unknown line" <> line


testInput :: String
testInput = [qc|A Y
B X
C Z|]
