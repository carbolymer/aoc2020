{-# LANGUAGE TemplateHaskell #-}
module Y2022.D11Spec where

import Common
import Control.Applicative.Combinators
import Control.Lens
import Control.Monad.State.Strict
import Data.Foldable                   (for_)
import Data.Functor
import Test.Hspec
import Text.InterpolatedString.Perl6
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer      qualified as L
import Data.List (sort, nub)

data Val = Item | Number Int
  deriving (Eq, Ord, Show)

data Operation
  = Add Val Val
  | Mul Val Val
  deriving (Eq, Ord, Show)

data Monkey = Monkey
  { _monkeyId           :: Int
  , _items              :: [Int]
  , _operation          :: Operation
  , _divTestDenominator :: Int
  , _divTestTarget      :: (Int, Int)
  , _inspectCount       :: Int
  } deriving (Eq, Ord, Show)
makeLenses ''Monkey

-- {{{ PARSER

parseMonkeys :: String -> [Monkey]
parseMonkeys = parseThrow (monkeyP `sepBy` eol)

monkeyP :: Parser Monkey
monkeyP = do
  _monkeyId <- string "Monkey " *> L.decimal <* char ':' <* eol :: Parser Int
  _items <- string "  Starting items: " *> itemListP <* eol
  _operation <- string "  Operation: new = " *> operationP <* eol
  _divTestDenominator <- string "  Test: divisible by " *> L.decimal <* eol
  tt1 <- string "    If true: throw to monkey " *> L.decimal <* eol
  tt2 <- string "    If false: throw to monkey " *> L.decimal <* eol
  let _divTestTarget = (tt1, tt2)
      _inspectCount = 0
  pure Monkey{..}
    where
      itemListP = L.decimal `sepBy` (string ", " :: Parser String) :: Parser [Int]
      operationP = do
        arg1 <- valP
        op' <- (string " * " $> Mul) <|> (string " + " $> Add)
        op' arg1 <$> valP
      valP = (string "old" $> Item) <|> (Number <$> L.decimal) :: Parser Val
-- }}}

calculateMonkeyBusiness :: Int -> Bool -> [Monkey] -> Int
calculateMonkeyBusiness n divBy3 monkeys = product . take 2 . reverse . sort . map _inspectCount $ runInspectionRounds n divBy3 monkeys

runInspectionRounds :: Int -> Bool -> [Monkey] -> [Monkey]
runInspectionRounds n divBy3 monkeys = (`execState` monkeys) $ replicateM n (runInspectionRound divBy3)

runInspectionRound :: Bool -> State [Monkey] ()
runInspectionRound divBy3 = do
  nrOfMonkeys <- gets length
  divisors <- product . nub <$> gets ((3:) . map _divTestDenominator)
  for_ ([0..(nrOfMonkeys - 1)] :: [Int]) $ \i -> do
    monkey <- (^?! element i) <$> get
    modify' $
      element i . items .~ []
    modify' $
      element i . inspectCount %~ ((length $ monkey ^. items) +)
    for_ (monkey ^. items) $ \item -> do
      let increasedWorry = evalOperation (monkey ^. operation) item
          worry = if divBy3 then increasedWorry `div` 3 else increasedWorry
          targetMonkey =
            if worry `mod` (monkey ^. divTestDenominator) == 0
              then monkey ^. divTestTarget . _1
              else monkey ^. divTestTarget . _2
      modify' $
        element targetMonkey . items %~ (worry `mod` divisors:)

evalOperation :: Operation -> Int -> Int
evalOperation (Add Item Item) item            = item + item
evalOperation (Add (Number n) Item) item      = n + item
evalOperation (Add Item (Number n)) item      = n + item
evalOperation (Add (Number n1) (Number n2)) _ = n1 + n2
evalOperation (Mul Item Item) item            = item * item
evalOperation (Mul (Number n) Item) item      = n * item
evalOperation (Mul Item (Number n)) item      = n * item
evalOperation (Mul (Number n1) (Number n2)) _ = n1 * n2


sampleInput :: String
sampleInput = [qc|Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1
|]

spec :: Spec
spec = do
  describe "validate sample input" $ do
    it "should parse sample input" $ do
      parseMonkeys sampleInput `shouldBe`
       [ Monkey
         { _monkeyId = 0
         , _items = [79 , 98]
         , _operation = Mul Item (Number 19)
         , _divTestDenominator = 23
         , _divTestTarget = (2 , 3)
         , _inspectCount = 0}
       , Monkey
         { _monkeyId = 1
         , _items = [54 , 65 , 75 , 74]
         , _operation = Add Item (Number 6)
         , _divTestDenominator = 19
         , _divTestTarget = (2 , 0)
         , _inspectCount = 0}
       , Monkey
         { _monkeyId = 2
         , _items = [79 , 60 , 97]
         , _operation = Mul Item Item
         , _divTestDenominator = 13
         , _divTestTarget = (1 , 3)
         , _inspectCount = 0}
       , Monkey
         { _monkeyId = 3
         , _items = [74]
         , _operation = Add Item (Number 3)
         , _divTestDenominator = 17
         , _divTestTarget = (0 , 1)
         , _inspectCount = 0}]
    it "run one inspection round" $ do
      map _items (runInspectionRounds 1 True $ parseMonkeys sampleInput) `shouldBe`
        [[26,27,23,20],[25,167,207,401,1046,2080],[],[]]
    it "get inspection counts p1" $ do
      map _inspectCount (runInspectionRounds 20 True $ parseMonkeys sampleInput) `shouldBe`
        [101, 95, 7, 105]
    it "get inspection counts p2 - 1" $ do
      map _inspectCount (runInspectionRounds 1 False $ parseMonkeys sampleInput) `shouldBe`
        [2,4,3,6]
    it "get inspection counts" $ do
      map _inspectCount (runInspectionRounds 10000 False $ parseMonkeys sampleInput) `shouldBe`
        [52166,47830,1938,52013]
    it "sample part 1" $ do
      calculateMonkeyBusiness 20 True (parseMonkeys sampleInput) `shouldBe` 10605
    it "sample part 2" $ do
      calculateMonkeyBusiness 10000 False (parseMonkeys sampleInput) `shouldBe` 2713310158

  describe "run puzzle input" $ do
    input <- runIO $ readFile "resources/2022/d11.txt"
    it "---PART ONE---" $ do
      calculateMonkeyBusiness 20 True (parseMonkeys input) `shouldBe` 110885
    it "---PART TWO---" $ do
      calculateMonkeyBusiness 10000 False (parseMonkeys input) `shouldBe` 25272176808
