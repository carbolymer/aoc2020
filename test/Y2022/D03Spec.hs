{-# LANGUAGE ViewPatterns #-}
module Y2022.D03Spec where

import Common
import Test.Hspec
import Data.Set (Set, difference, intersection)
import GHC.Exts (IsList(..))
import Text.InterpolatedString.Perl6
import Debug.Trace (traceShowId)
import Data.List.Split (chunksOf)

spec :: Spec
spec = do
  describe "" $ do
    it "part1" $ do
      sum (parseRucksacks testInput) `shouldBe` 157
    it "part2" $ do
      sum (parseRucksacks2 testInput) `shouldBe` 70

  describe "run puzzle input" $ do
    input <- runIO $ readFile "resources/2022/d03.txt"
    it "---PART ONE---" $ do
      sum (parseRucksacks input) `shouldBe` 7990
    it "---PART TWO---" $ do
      sum (parseRucksacks2 input) `shouldBe` 2602

parseRucksacks :: String -> [Int]
parseRucksacks =  map (checkSinglePrio . fmap itemToPriority . toList . distinctItems . splitCompartments) . lines

checkSinglePrio :: [Int] -> Int
checkSinglePrio [p] = p
checkSinglePrio ps = error $ "more than one priority out :" <> show ps


distinctItems :: (String, String) -> Set Char
distinctItems (r1, r2) = intersection (fromList r1) (fromList r2)

splitCompartments :: String -> (String, String)
splitCompartments input = splitAt (length input `div` 2) input

itemToPriority :: Char -> Int
itemToPriority (fromEnum -> cv)
  | 65 <= cv && cv <= 90 = cv - 38 -- uppercase
  | 97 <= cv && cv <= 122 = cv - 96 -- lowercase
  | otherwise = error $ "unknown priority " <> [toEnum cv]

parseRucksacks2 :: String -> [Int]
parseRucksacks2 = map (checkSinglePrio . map itemToPriority . toList . foldl intersection allLetters . map fromList) . chunksOf 3 . lines

allLetters :: Set Char
allLetters = fromList . map toEnum $ [65..90] <> [97..122]

testInput :: String
testInput = [qc|vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw|]
