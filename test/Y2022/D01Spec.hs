module Y2022.D01Spec where

import Common
import Data.List
import Debug.Trace
import Test.Hspec
import Text.InterpolatedString.Perl6

spec :: Spec
spec = do
  describe "" $ do
    it "read sample input" $ do
      topCalories (readItems testInput) `shouldBe` 24000

  describe "run puzzle input" $ do
    input <- runIO $ readItems <$> readFile "resources/2022/d01.txt"
    it "---PART ONE---" $ do
      topCalories input `shouldBe` 68775
    it "---PART TWO---" $ do
      sumTop3Calories input `shouldBe` 202585

topCalories :: [Int] -> Int
topCalories = foldl max 0

sumTop3Calories :: [Int] -> Int
sumTop3Calories = sum . foldl top3Max [0, 0, 0]
  where
    top3Max :: [Int] -> Int -> [Int]
    top3Max [m1, m2, m3] e
      | e > m1 = [e, m1, m2]
      | e > m2 = [m1, e, m2]
      | e > m3 = [m1, m2, e]
      | otherwise = [m1, m2, m3]
    top3Max a _ = error $ "wtf " <> show  a


readItems :: String -> [Int]
readItems = map sum . fmap (fmap read) . filter (/= [""]) .  groupBy neitherEmpty . lines
  where
    neitherEmpty "" _ = False
    neitherEmpty _ "" = False
    neitherEmpty _ _  = True


testInput :: String
testInput = [qc|1000
2000
3000

4000

5000
6000

7000
8000
9000

10000|]
