module Y2022.D17Spec where

import Common
import Test.Hspec
import Text.InterpolatedString.Perl6

spec :: Spec
spec = do
  describe "validate sample input" $ do
    it "sample part 1" $ do
      True `shouldBe` True

  describe "run puzzle input" $ do
    -- input <- runIO $ readFile "resources/2022/d17.txt"
    it "---PART ONE---" $ do
      True `shouldBe` True
    it "---PART TWO---" $ do
      True `shouldBe` True

sampleInput :: String
sampleInput = [qc||]

